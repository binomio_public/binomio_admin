module BinomioAdmin
  class Editable < ApplicationRecord
    validates :name, presence: true, uniqueness: true, length: { maximum: 128 }
    validates :value, length: { maximum: 16384 }
    has_one_attached :image # Optionally, for image fields
    has_one_attached :download # Optionally, for download fields

    # Given a name, get the associated Editable if it doesn't exist, a new one if it doesn't
    def self.find_or_create name
      self.find_by_name(name) || Editable.new(name: name, value: nil)
    end

    # Given a name, get the value associated to its Editable if it exists, or nil if it doesn't
    def self.value_of name
      editable = self.find_by_name(name)
      editable ? editable.value : nil
    end

    # Returns true if there's either an image or download attached to this editable
    def attached?
      image.attached? || download.attached?
    end
  end
end