require 'open-uri'
module BinomioAdmin
  class EditablesController < ApplicationController
    before_action :authorize_user, except: [:download]
    SEPARATOR = "%"
    HOST = Rails.configuration.host

    # Shows all Editables in a hierarchical view
    def index
      @tree = {}
      Editable.find_each do |editable|
        @tree.nested_set(editable.name.split("."), editable)
      end
    end

    # Given the key of an editable download, redirect to the download page for it
    def download
      editable = Editable.find_by_name params[:name]
      render plain: "La descarga solicitada no existe" and return if editable.blank?
      render plain: "La descarga solicitada no está disponible" and return unless editable.download.attached?
      redirect_to rails_blob_path(editable.download, disposition: "inline")
    end

    # Method called by the editor to mass-update all fields
    def update
      text_fields = params.to_unsafe_h[:textFields] # Name format: name
      image_fields = params.to_unsafe_h[:imageFields] # Name format: EntityClass%id%attribute_name
      image_collection_fields = params.to_unsafe_h[:collectionImageFields] # Name format: EntityClass%id%attribute_name%attachment_id
      download_fields = params.to_unsafe_h[:downloadFields]
      render json: { message: "No hay campos para actualizar" }, status: 400 and return if text_fields.blank? && image_fields.blank? && image_collection_fields.blank? && download_fields.blank?
      begin
        Editable.transaction do
          update_text_fields(text_fields) if text_fields.present?
          update_image_fields(image_fields) if image_fields.present?
          update_image_collection_fields(image_collection_fields) if image_collection_fields.present?
          update_download_fields(download_fields) if download_fields.present?
        end
      rescue Exception => ex
        puts "#{ex.class}: #{ex.message}"
        puts ex.backtrace.join("\n")
        render json: { message: "Ocurrió un error: #{ex.message}. No se han aplicado cambios." }, status: 400 and return
      end
      render json: { message: "Actualizado exitosamente" }, status: 200
    end

    private

    # Mass-update all text fields
    def update_text_fields fields
      fields.each do |key, value|
        if key_is_for_an_entity? key
          entity, attribute_name = key_to_entity key
          entity.public_send("#{attribute_name}=", value)
          entity.save!
        else
          editable = Editable.find_or_create key
          editable.value = value
          editable.save!
        end
      end
    end

    # Mass-update all image fields (not including collections)
    def update_image_fields fields
      fields.each do |key, value|
        next if value.starts_with?("/rails") || value.include?(HOST) # User didn't change this image, so we skip it
        io = BinomioAdmin::Images.transform_and_convert_to_io value, key
        if key_is_for_an_entity? key
          entity, attribute_name = key_to_entity key
          attachment = entity.public_send("#{attribute_name}")
        else
          entity = Editable.find_or_create key
          attachment = entity.image
        end
        attachment.attach(io: io, filename: "image")
        entity.save!
      end
    end

    # Mass-update all image collections
    # Receives a list of fields like: {
    #   "CoreModel%50%slider_images%15" => "/someurl", # Existing attachment with ID 15, we ignore
    #   "CoreModel%50%slider_images%0" => "data:image/jpeg..." # New attachment (ID 0), we add
    # }
    def update_image_collection_fields fields
      # TODO: rewrite to accept external urls
      collections = {}
      fields.each do |key, value|
        parts = key.rpartition SEPARATOR # "a%b%c%d" => ["a%b%c", "%", "d"]
        collections[parts[0]] = [] if collections[parts[0]].nil?
        collections[parts[0]] << { id: parts[2].to_i, dataurl: value }
      end
      collections.each do |key, new_attachments|
        entity, attribute_name = key_to_entity key
        old_attachments = entity.public_send(attribute_name)
        # Old attachments not sent in the request are assumed to be deleted by the user, so we purge them
        old_attachments.each do |old_attachment|
          unless new_attachments.any? { |a| a[:id] == old_attachment.id }
            old_attachment.purge
          end
        end
        # New attachments not present should be added to the collection
        new_attachments.each do |new_attachment|
          if new_attachment[:dataurl].starts_with? "data:"
            unless old_attachments.any? { |a| a.id == new_attachment[:id] }
              old_attachments.attach(io: src_to_io(new_attachment[:dataurl]), filename: "image")
            end
          end
        end
      end
    end

    # Mass-update all downloads, working in the same way as update_image_fields
    def update_download_fields fields
      fields.each do |key, value|
        if key_is_for_an_entity? key
          entity, attribute_name = key_to_entity key
          attachment = entity.public_send("#{attribute_name}")
          filename = "download"
        else
          entity = Editable.find_or_create key
          attachment = entity.download
          filename = entity.value || "download"
        end
        attachment.attach(io: src_to_io(value), filename: filename)
        entity.save!
      end
    end

    # Returns true if the key refers to an entity's attribute, or false if it's just an Editable
    def key_is_for_an_entity? key
      key.include? SEPARATOR
    end

    # When you use the 'editable' helper with an entity, the editable field contains a key that identifies the entity
    # It looks like 'Book%123%author', where the format is 'Class%ID%attribute'
    # This method returns the entity that a key refers to, and also the attribute name
    def key_to_entity key
      parts = key.split SEPARATOR
      raise "Invalid key: #{key}" if parts.length != 3
      entity_class = Object.const_get parts[0]
      entity_id = parts[1].to_i
      attribute_name = parts[2]
      return entity_class.find(entity_id), attribute_name
    end

    # Converts an <img> src into an IO object so that we can attach it
    def src_to_io url
      if url.starts_with? "data" # If the src is a dataurl with a base64-encoded image
        StringIO.new(Base64.decode64(url.split(",")[1]))
      else # If the src is an external image URL
        begin
          result = open(url) # May return either a Tempfile or StringIO
          return result.class == Tempfile ? File.open(result.path) : result
        rescue Exception => ex
          puts "#{ex.class}: #{ex.message}"
          raise "No se pudo obtener la imagen: #{url}"
        end
      end

    end

    # This method is called before all edition actions. The implementing project is
    # encouraged to override it with custom authorizations if desired.
    def authorize_user
      authenticate_user!
    end
  end
end
class Hash
  # Set the value of a nested hash even if the parents don't exist. For example, given a list
  # of keys like [:a, :b, :c] and a value (like 27), make it so that self[:a][:b][:c] = 27
  def nested_set keys, value
    if keys.length == 1
      self[keys[0]] = value
    else
      self[keys[0]] = {} unless self.include? keys[0]
      self[keys[0]].nested_set keys[1..-1], value
    end
  end
end