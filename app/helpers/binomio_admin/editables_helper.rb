module BinomioAdmin
  module EditablesHelper
    DEFAULT_MISSING_IMAGE = "binomio_admin/missing_image_square.svg"
    MISSING_TEXT = "Insertar texto aquí"
    
    # Generates an editable field for a given entity's text attribute
    def editable_text entity, attribute, options = {}, html_options = {}
      attributes = {
        class: "editable-text #{html_options[:class]}",
        "data-key": entity_attr_to_key(entity, attribute),
        "data-changed": false,
        "data-notes": options[:notes]
      }
      content_tag :span, (entity.public_send(attribute) || MISSING_TEXT).to_s.html_safe, html_options.merge(attributes)
    end

    # Generates en editable field stored as an Editable entity in database
    def editable_text_named name, options = {}, html_options = {}
      attributes = {
        class: "editable-text #{html_options[:class]}",
        "data-key": name,
        "data-changed": false,
        "data-notes": options[:notes],
        "data-size": options[:size]
      }
      content_tag :span, (Editable.value_of(name) || MISSING_TEXT).to_s.html_safe, html_options.merge(attributes)
    end

    # Generates an editable field for a given entity's image attribute with ActiveStorage
    def editable_image entity, attribute, options = {}, html_options = {}
      attachment = entity.public_send(attribute)
      html_options[:class] = "editable-image #{html_options[:class]}"
      html_options[:"data-key"] = entity_attr_to_key(entity, attribute)
      html_options[:"data-changed"] = false
      image_tag get_url(attachment, options[:missing_image]), html_options
    end

    # Generates an editable image field stored as an Editable entity with ActiveStorage attachent
    def editable_image_named name, options = {}, html_options = {}
      attachment = Editable.find_or_create(name).image
      html_options[:class] = "editable-image #{html_options[:class]}"
      html_options[:"data-key"] = name
      html_options[:"data-changed"] = false
      image_tag get_url(attachment, options[:missing_image]), html_options
    end

    # Generates an editable image field for one of a given entity's image attachments
    # If the attachent is nil, it'll be interpreted as a blank field to upload a new image to the collection
    def editable_collection_image entity, attribute, attachment, options = {}, html_options = {}
      html_options[:class] = "editable-collection-image #{html_options[:class]}"
      html_options[:"data-key"] = entity_item_to_key(entity, attribute, attachment)
      url = get_url attachment, options[:missing_image]
      image_tag url, html_options
    end

    # Generates an editable download field for a given entity's attachment attribute
    # You can pass a custom HREF, or let ActiveStorage generate one for you by default
    def editable_download entity, attribute, options = {}, html_options = {}
      attributes = {
        href: html_options[:href] || get_url(entity.public_send(attribute), options[:missing_file]),
        target: "_blank",
        class: "editable-download #{html_options[:class]}",
        "data-key": entity_attr_to_key(entity, attribute),
        "data-changed": false
      }
      content_tag :a, html_options.merge(attributes) do yield end
    end

    # Generates an editable download field stored as an Editable entity with an ActiveStorage attachment
    # You can pass a custom HREF, or let ActiveStorage generate one for you by default
    def editable_download_named name, options = {}, html_options = {}
      attributes = {
        href: html_options[:href] || ba_download_path(name: name),
        target: "_blank",
        class: "editable-download #{html_options[:class]}",
        "data-key": name,
        "data-changed": false
      }
      content_tag :a, html_options.merge(attributes) do yield end
    end

    # Given the name of an editable, return its associated image's URL
    def name_to_url name, missing_file = nil
      get_url Editable.find_or_create(name).image, missing_file
    end

    # Generate the HTML of editable name tree that's shown on the editable index page
    def display_tree node, level = 0
      return node.sort.map{|key, child_node|
        if child_node.is_a? Hash
          "<div style='padding-left:#{level * 20}px'>⚫ <b>#{key}</b>:</div>" + display_tree(child_node, level + 1)
        else
          if child_node.image.attached?
            value = editable_image_named child_node.name, {}, style: "max-height: 18px;"
          elsif child_node.download.attached?
            value = editable_download_named child_node.name do content_tag :span, "📄" end
          elsif child_node.value == ""
            value = editable_download_named child_node.name do content_tag :span, "❎" end
          else
            value = editable_text_named child_node.name
          end
          "<div style='padding-left:#{level * 20}px'>⚫ <b>#{key}</b>: #{value}</div>"
        end
      }.join
    end

    private

    # Given an ActiveStorage Attachment/Attached::Many/Attached::One, return its accessible URL
    def get_url attachment, missing_image = DEFAULT_MISSING_IMAGE
      missing_image ||= DEFAULT_MISSING_IMAGE # In case nil is passed
      if attachment.nil?
        image_path(missing_image)
      elsif attachment.class == ActiveStorage::Attachment
        url_for(attachment)
      else
        attachment.attached? ? url_for(attachment) : image_path(missing_image)
      end
    end

    # Generate a string key to represent an entity's attribute
    def entity_attr_to_key entity, attribute
      "#{entity.class.name}%#{entity.id}%#{attribute}"
    end

    # Generate a string key to represent an attachment within an entity's collection attribute
    def entity_item_to_key entity, attribute, attachment
      "#{entity.class.name}%#{entity.id}%#{attribute}%#{attachment.present? ? attachment.id : 0}"
    end
  end
end