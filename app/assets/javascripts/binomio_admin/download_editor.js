var downloadPopper; // The Popper object from popper.js that appears when editing an download
var selectedDownload; // The DOM element that stores the base-64 encoded download as an attribute. Can be any tag.
$(() => {
  // Hide popup when clicking outside of it
  $(document).on('click', function(e) {
    if (e.button == 0 && !$(e.target).hasClass("editable-download") && !$.contains($("#divDownloadPopper")[0], e.target)) {
      unselectDownload();
    }
  });
  // Make all editable downloads editable through a popup
  editableDownload($('.editable-download'));
  // Prettier upload button clicks a hidden file input
  $("#btnUploadDownload").click(() => {
    $("#filUploadDownload").click();
  });
  // When a new file is chosen, store it as an attribute of the container tag
  $("#filUploadDownload").change((evt) => {
    var reader = new FileReader();
    reader.readAsDataURL($("#filUploadDownload")[0].files[0]);
    reader.onload = (evt) => {
      selectedDownload.dataset.download = evt.target.result;
      $(selectedDownload).attr("data-changed", "true");
      setDirty(true);
      unselectDownload();
    };
  });
});

/* Make editable downloads generate the edition popup when clicked */
function editableDownload(elements) {
  elements.click((evt) => {
    if (evt.shiftKey) return;
    evt.preventDefault();
    evt.stopPropagation();
    selectDownload(evt.currentTarget);
  })
}

/* Given an editable download DOM element, make the download edition popper appear for it */
function selectDownload(element) {
  unselectAllEditables();
  selectedDownload = element;
  downloadPopper = Popper.createPopper(selectedDownload, $("#divDownloadPopper")[0]);
  $("#divDownloadPopper").show();
  $("#btnDownloadDownload").attr("href", element.href);
}

/* Make download no longer be selected for edition, hiding popup */
function unselectDownload() {
  selectedDownload = null;
  $("#divDownloadPopper").hide();
}