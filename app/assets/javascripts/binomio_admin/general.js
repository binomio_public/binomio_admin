var dirty = false; // Editor is dirty if it has unsaved changes
var glowEnabled = false; // If this is enabled, all editable components will glow to highlight them to the user
$(document).ready(() => {
  setDirty(false);
  // Make 'Publish changes' button send changes to server
  $("#btnSaveEdits").click(() => {
    var button = $("#btnSaveEdits");
    setButtonState(button, "waiting");
    $.ajax({
      url: "/ba/editable",
      type: 'patch',
      data: JSON.stringify(getEditableData()),
      contentType: "application/json",
      success: () => {
        setButtonState(button, "normal");
        $(".editable-text, .editable-image, .editable-collection-image, .editable-download").attr("data-changed", "false");
        setDirty(false);
      },
      error: () => {
        setButtonState(button, "normal");
      },
      complete: (xhr) => {
        if (xhr.responseJSON) {
          showMessage(xhr.responseJSON.message);
        } else {
          try {
            message = JSON.parse(xhr["responseText"])["message"] || xhr["responseText"];
          } catch (ex) {
            message = "Lo sentimos, ocurrió un problema inesperado.";
          }
          showMessage(message);
        }
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
      }
    });
  });
  // Make 'Discard' button reload the page
  $("#btnDiscardEdits").click(() => window.location.reload(true));
  // Toggle glow when clicking on its button
  $("#btnGlow").click((evt) => {
    evt.preventDefault();
    glowEnabled = !glowEnabled;
    $(".editable-text, .editable-image, .editable-collection-image, .editable-download").toggleClass("glow", glowEnabled);
  });
  // Allow the user to re-show the text fields they hid, which otherwise would remain impossible to edit
  $("#btnResetEmptyText").click(evt => {
    evt.preventDefault();
    resetEmptyTextFields();
  });
  // Warn users before leaving when they have unsaved changes
  window.addEventListener("beforeunload", function (e) {
    if (!dirty) return undefined;
    var confirmationMessage = 'You have unsaved changes. Leave anyways?';
    (e || window.event).returnValue = confirmationMessage; //Gecko + IE
    return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
  });
});
// Grab the contents of all editable fields and put them into a name => value hash
function getEditableData() {
  var textFields = {}, imageFields = {}, collectionImageFields = {}, downloadFields = {};
  $(".editable-text").each((i,e) => {
    if (e.dataset.changed != "true") return;
    textFields[$(e).data("key")] = $(e).html();
  });
  $(".editable-image").each((i,e) => {
    if (e.dataset.changed != "true") return;
    if (e.tagName == "IMG") {
      imageFields[$(e).data("key")] = $(e).prop("src");
    } else { /* Div with background-image */
      imageFields[$(e).data("key")] = $(e).css("background-image").replace(/url\("(.*?)"\)/, "$1") // Convert 'url("a")' to 'a'
    }
  });
  $(".editable-collection-image").each((i,e) => {
    collectionImageFields[$(e).data("key")] = $(e).prop("src");
  });
  $(".editable-download").each((i,e) => {
    if (e.dataset.changed != "true") return;
    downloadFields[$(e).data("key")] = $(e).data("download");
  });
  return {
    textFields: textFields,
    imageFields: imageFields,
    collectionImageFields: collectionImageFields,
    downloadFields: downloadFields
  };
}
// Display a message on the edition mode footer (if short) or a modal (if long)
function showMessage(msg) {
  if (msg.length > 50) {
    alert(msg);
  } else {
    $("#divEditorCenter").html(msg);
  }
}
// Set whether there are unsaved changes to the current page
function setDirty(bool) {
  dirty = bool;
  $(".editor-button").prop("disabled", !bool);
}
// Unselect all editable elements of any type */
function unselectAllEditables() {
  unselectText();
  unselectImage();
  unselectDownload();
}
// Given an editable DOM element of unknown type, select it
function selectEditable(element) {
  if (element.className.includes("editable-text")) {
    selectText(element);
  } else if (element.className.includes("editable-image")) {
    selectImage(element);
  } else if (element.className.includes("editable-download")) {
    selectDownload(element);
  } else {
    console.error("Attempted to select an element that's not editable")
  }
}
// Enable the selection of images hidden behind other elements by right clicking over them
$(document).click(evt => {
  if (evt.ctrlKey) {
    var allClickedElements = document.elementsFromPoint(evt.clientX, evt.clientY);
    for (var elem of allClickedElements) {
      var e = $(elem);
      if (e.hasClass("editable-text") || e.hasClass("editable-image") || e.hasClass("editable-download")) {
        selectEditable(elem);
        return;
      }
    }
    evt.preventDefault();
    evt.stopPropagation();
  }
});