var imagePopper; // The Popper object from popper.js that appears when editing an image
var selectedImage; // The DOM element that represents the image being edited. Should be <img> or <div>.
$(() => {
  // Hide popup when clicking outside of it
  $(document).on('click', function(e) {
    if (e.button == 0 && !$(e.target).hasClass("editable-image") && !$.contains($("#divImagePopper")[0], e.target) && !$.contains($("#divUrlModal")[0], e.target)) {
      unselectImage();
    }
  });
  // Make all editable images editable through a popup
  editableImage($('.editable-image, .editable-collection-image'));
  // Prettier upload button clicks a hidden file input
  $("#btnUploadImage").click(() => {
    $("#filUploadImage").click();
  });
  // Trigger bootstrap modal asking for the external URL
  $("#btnUploadUrl").click(() => {
    $('#divUrlModal').modal();
    setTimeout(() => {
      $('#txtImageUrl').focus();
    }, 700);
  });
  // When a new file is chosen, display it in place of the previous image as a preview
  $("#filUploadImage").change((evt) => {
    var reader = new FileReader();
    reader.readAsDataURL($("#filUploadImage")[0].files[0]);
    reader.onload = (evt) => {
      replaceImageWith(evt.target.result);
    };
  });
  // When the user has entered an external image URL, display it
  $("#btnLoadUrl").click(() => {
    replaceImageWith($("#txtImageUrl").val());
    $("#txtImageUrl").val("");
    $('#divUrlModal').modal("hide");
  });
  // Shortcut: press enter on the external URL field to confirm
  $("#txtImageUrl").keypress(function(e) {
    if(e.which == 13) $("#btnLoadUrl").click();
  })
});

/* Make editable images generate the edition popup when clicked */
function editableImage(elements) {
  elements.click((evt) => {
    if (evt.shiftKey) return;
    evt.preventDefault();
    evt.stopPropagation();
    selectImage(evt.currentTarget);
  })
}

/* Given an editable img/div, make the image edition popper appear for it */
function selectImage(element) {
  unselectAllEditables();
  selectedImage = element;
  var popperIsInner = (selectedImage.width || selectedImage.offsetWidth) > 180; // Whether the popper should be shown inside the image instead of outside
  imagePopper = Popper.createPopper(selectedImage, $("#divImagePopper")[0], {
    modifiers: [
      {
        name: 'offset',
        options: {
          offset: [0, -80]
        }
      }
    ]
  });
  $("#divImagePopper").toggleClass("inner", popperIsInner);
  $("#divImagePopper").show();
  if (selectedImage.width) {
    $("#spnPopperSize").html(selectedImage.width + "x" + selectedImage.height);
  } else {
    $("#spnPopperSize").html("<i>Fondo</i>");
  }
}

/* Make image no longer be selected for edition, hiding popup */
function unselectImage() {
  selectedImage = null;
  $("#divImagePopper").hide();
}

// Given a dataurl (base64-encoded image), replace the currently selected image with it
function replaceImageWith(dataurl) {
  let imagesToReplace;
  // Update any other instances of the same image in this page too, unles it's a collection,
  // in which case two newly added images would share the same key.
  if (selectedImage.classList.contains("editable-collection-image")) {
    imagesToReplace = $(selectedImage);
  } else {
    imagesToReplace = $("*[data-key='" + selectedImage.dataset.key + "']");
  }
  imagesToReplace.each(function() {
    if (this.tagName == "IMG") {
      $(this).prop("src", dataurl);
    } else {
      $(this).css("background-image", "url('" + dataurl + "')");
    }
    $(this).attr("data-changed", "true");
  });
  unselectImage();
  setDirty(true);
}