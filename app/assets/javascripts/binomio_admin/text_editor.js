var textPopper; // The Popper object from popper.js that appears when editing a text
var selectedText; // The DOM element that represents the text being edited. Should be a <span>.
$(() => {
  // Hide popup when clicking outside of them
  $(document).on('click', function(e) {
    if (e.button == 0 && !$(e.target).hasClass("editable-text") && !$.contains($("#divTextPopper")[0], e.target)) {
      unselectText();
    }
  });
  // Make all editable fields editable through a popup
  editableText($(".editable-text"));

  $("#tarNewText").keydown((evt) => {
    if (!evt.shiftKey && evt.keyCode == 13) {
      $("#btnSaveText").click();
    }
  });
  $("#btnSaveText").click(() => {
    const selectedGroup = $(`*[data-key='${selectedText.dataset.key}']`); // If the same editable key appears on more than one DOM element, ensure that all of them are updated
    selectedGroup.html($("#tarNewText").val().replace(/\n/g, "<br />"));
    selectedGroup.attr("data-changed", "true");
    setDirty(true);
    unselectText();
  });
  $("#btnDiscardText").click(() => {
    unselectText();
  });
});

// Make editable texts generate the edition popup when clicked
function editableText(elements) {
  elements.click((evt) => {
    if (evt.shiftKey) return;
    evt.preventDefault();
    evt.stopPropagation();
    selectText(evt.currentTarget);
  });
}

/* Given an editable span, highlight it and make the text edition popper appear */
function selectText(element) {
  unselectAllEditables();
  selectedText = element;
  textPopper = Popper.createPopper(selectedText, $("#divTextPopper")[0], {
    placement: "bottom"
  });
  var textarea = $("#tarNewText");
  $("#spnTextNotes").html($(element).data("notes") || "");
  $("#divTextPopper").show();
  textarea.val($(selectedText).html().replace(/<br ?\/?>/g, "\n"));
  textarea.height(20);
  textarea.height(textarea[0].scrollHeight);
  $(".selected-text").removeClass("selected-text");
  $(selectedText).addClass("selected-text");
  setTimeout(() => $("#tarNewText").focus(), 100);
}

/* Make currently selected text no longer be selected for edition, hiding popup */
function unselectText() {
  selectedText = null;
  $("#divTextPopper").hide();
  $(".selected-text").removeClass("selected-text");
  $(selectedText).removeClass("selected-text");
}

/* If the user enters a blank string in a field, that field will disappear. This method
allows the user to make these fields reappear again with a placeholder text. */
function resetEmptyTextFields() {
  $(".editable-text").each(function() {
    if (this.innerHTML == "") {
      this.innerHTML = "Texto";
    }
  });
}