require "mini_magick"

module BinomioAdmin
  module Images

    # Given a data URL corresponding to a Base64-encoded image, do 2 things:
    # 1) Apply all the transformations defined by the user for the given key (see self.transform)
    # 2) Return an object compatible with the ActiveStorage attach(io: ...) method
    def self.transform_and_convert_to_io encoded_image, editable_key
      transformations = transformations_for(editable_key)
      if transformations.present?
        # ImageMagick needs to work on a file on disk, so we must write the encoded
        # image to disk before processing it.
        tempfile = Tempfile.new("ba_upload_#{SecureRandom.hex 8}")
        tempfile.write Base64.decode64(encoded_image.split(",")[1]).force_encoding("UTF-8")
        transform(tempfile, editable_key, transformations)
        # The returned IO will read the file from disk
        return File.open(tempfile.path)
      else
        # We just return an IO directly from the string, since there's no need to persist on disk
        StringIO.new(Base64.decode64(encoded_image.split(",")[1]))
      end
    end

    # Given a Tempfile corresponding to an image, apply all the transformations
    # defined on binomio_admin.yml (if any), such as resizing and converting the format,
    # replacing the original file with the transformed version
    def self.transform tempfile, editable_key, transformations
      if transformations.present?
        original_path = tempfile.path
        mini_image = MiniMagick::Image.new original_path.dup

        if transformations["format"]
          mini_image.format transformations["format"]
        end

        if transformations["resize"]
          mini_image.resize transformations["resize"]
        end

        # If the format conversion process changed its extension, move the file back to its original filename
        if original_path != mini_image.path
          if File.exists?(mini_image.path) && !File.exists?(original_path)
            FileUtils.mv mini_image.path, original_path
          end
        end
      end
    end

    # Return which transformations the user has defined for the given editable_key, as defined on the YML
    def self.transformations_for editable_key
      if editable_key.include? '%'
        parts = editable_key.split('%')
        raise ArgumentError.new("Expected key '#{editable_key}' to have at least 3 parts when split by '%'") if parts.length < 3
        return BinomioAdmin.config['model_editables']["#{parts[0].downcase}.#{parts[2]}"]
      else
        return BinomioAdmin.config['named_editables'][editable_key]
      end
    end
  end
end