require 'thor'
require 'binomio_admin/generators/migration'

module BinomioAdmin
  class CLI < Thor

    desc "migration", "Generates the migration that creates the table for Editables"
    def migration
      BinomioAdmin::Generators::Migration.start
    end

  end
end
