class CreateEditables < ActiveRecord::Migration[5.2]
  def change
    create_table :editables do |t|
      t.timestamps
      t.string :name
      t.string :value
      t.index :name, unique: true
    end
  end
end
