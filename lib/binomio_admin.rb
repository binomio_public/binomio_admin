require "binomio_admin/version"
require "binomio_admin/engine"
require "binomio_admin/images"

CONFIG_PATH = Pathname.new(Dir.getwd).join("config/binomio_admin.yml")

module BinomioAdmin
  @@config = nil

  def self.load_config
    if File.exists? CONFIG_PATH
      @@config = YAML.load_file(CONFIG_PATH)
    end
  end

  def self.config
    @@config
  end
end
