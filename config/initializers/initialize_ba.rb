BinomioAdmin.load_config

Rails.application.config.assets.precompile += [
  'binomio_admin/missing_image_square.svg',
  'binomio_admin/missing_image_3x1.svg',
  'binomio_admin/all.css',
  'binomio_admin/all.js'
]