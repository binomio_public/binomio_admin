Rails.application.routes.draw do
  get '/ba/download' => 'binomio_admin/editables#download', as: :ba_download
  get '/ba/editables' => 'binomio_admin/editables#index', as: :ba_editables_index
  patch '/ba/editable' => 'binomio_admin/editables#update'
  get '/ba/editor' => 'binomio_admin/editables#help', as: :ba_help
end